import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';


/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {

    redirectUri: string = "urn:ietf:wg:oauth:2.0:oob";
    clientId: string = "065b5e91-8d36-4f87-9c8f-bfa9fbf200ba";
    clientSecret: string = "vQ4v]E2T7!U68HUbI8,4S`}#";
    loginUrl: string = "https://partners-login.eliotbylegrand.com/authorize?client_id=" + this.clientId + "&redirect_uri=" + this.redirectUri + "&response_type=code";
    registerUrl: string = "https://login.microsoftonline.com/eliotclouduamprd.onmicrosoft.com/oauth2/v2.0/authorize?p=b2c_1_eliot-signup&client_id=c3b23ef7-aa7d-4d3a-8253-efbb296457cd&redirect_uri=https%3a%2f%2fdeveloper.legrand.com%2fsignin-aad&response_mode=form_post&response_type=id_token&scope=openid&state=OpenIdConnect.AuthenticationProperties%3dji7YPanCHbzVvz8TkUEk_3Htj0uezKMwiMHsjTy7PbXWubkbJsciucQ2xxAilL0t8UWQSvM0iNMowU273udef1sVRId_KBOAv9cq4L1XjT9AV-hMSWtASJRx1ddRkYUeq9G5E1RWqUiXx2OgU9N2XYrUL9pFFNW_-cwVkfIUPW0JJs3M-tOO3sgpxMX3six8pi6J73z_ZvSRFidBj1xfbnIEh3kGhVx-23mIeYY1CXoz3IRIEgdNkRd8n5HumEV3FWjDNAshh16iqABnG5f8AloidVMeTlLfh54d-sp2ETc&nonce=636662264575452392.NTlhMjk5ZDgtNjU4Ni00NTdhLTlkMzYtMzFkZWYyOGEyNWIwMmZhNjE5MDQtMjFiNC00MjFiLWEwOGEtNjk1YzZlNmVkY2Ex";

    constructor(
        public http: Http,
        public iab: InAppBrowser,
        public storage: Storage) {

    }

    register() {
        return new Promise((resolve, reject) => {
            const options: InAppBrowserOptions = {
                zoom: 'no',
                clearsessioncache: 'yes'
            };
            const browser = this.iab.create(this.registerUrl, '_self', options);
            browser.on('loadstop').subscribe(evt => {
                resolve(evt);
            });

        });
    }

    login() {
        return new Promise((resolve, reject) => {     
            const options: InAppBrowserOptions = {
                zoom: 'no',
                clearsessioncache: 'yes'
            };
            const browser = this.iab.create(this.loginUrl, '_self', options);
    
            browser.on('loadstart').subscribe(evt => {
                if (evt.url.startsWith('http://urn:ietf:wg:oauth:2.0:oob')) {
                    let code = evt.url.replace('http://urn:ietf:wg:oauth:2.0:oob?code=', '');
                    browser.close();
    
                    resolve(code);
                }
            });
        });

    }

    getToken(code) {
        return new Promise(resolve => {
        
            let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            
            let body = {
                client_id: this.clientId,
                grant_type: 'authorization_code',
                code: code,
                client_secret: this.clientSecret
            };
            
            let formData = [];

            for (const property in body) {
                const encodedKey = encodeURIComponent(property);
                const encodedValue = encodeURIComponent(body[property]);
                formData.push(encodedKey + '=' + encodedValue);
            }
            
            this.http.post("https://partners-login.eliotbylegrand.com/token", formData.join("&"), {headers: headers}).subscribe(response => {
                resolve(response.json());
            });
        });
    }

}
