import { Http, Headers } from '@angular/http';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {

    calcMetrics: any;
    accessToken: string;

    constructor(
        public http: Http,
        public loadingCtrl: LoadingController,
        public storage: Storage) {

    }

    getCalcMetrics() {
        return new Promise((resolve, reject) => {
            let loading = this.loadingCtrl.create({'content': 'Please wait...', 'enableBackdropDismiss': true});
            loading.present();

            this.storage.get('access_token').then(accessToken => {

                let headers = new Headers({
                    'Authorization': 'bearer ' + accessToken
                });
                
                this.http.get('http://192.168.43.38:11080/Atlantis/calculated/metrics/getjson', {headers: headers}).subscribe(response => {
                    resolve(response.json());
                    loading.dismiss();
                }, error => {
                    reject(error);
                    loading.dismiss();
                });
            });
        });
    }

    device(mode: string) {
        return new Promise((resolve, reject) => {

            let loading = this.loadingCtrl.create({'content': 'Sending command...', 'enableBackdropDismiss': true});
            loading.present();

            this.http.get('http://192.168.43.220/' + mode.toUpperCase()).subscribe(response => {
                loading.dismiss();
                resolve(response);
            }, error => {
                loading.dismiss();
                reject(error);
            });
        });
    }
}
