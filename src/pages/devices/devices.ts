import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ShowDevicePage } from '../../pages/show-device/show-device';

/**
 * Generated class for the DevicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-devices',
    templateUrl: 'devices.html',
})
export class DevicesPage {

    devices: any = [];
    userToken: string;
    accessToken: string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public menuCtrl: MenuController,
        public storage: Storage) {

        for (let i = 1; i <= 6; i++) {
            
            let device = {
                name: 'device' + i,
                id: i,
                type: Math.floor(Math.random() * 2) + 1 == 1 ? 'temperature' : 'hygrometry',
                battery: Math.floor(Math.random() * (100 - 1) + 1),
                value: 0,
                unit: ''
            };

            if (device.type === 'temperature') {
                device.value = Math.floor(Math.random() * (21 - 15) + 15);
                device.unit = '°C'
            } else {
                device.value = Math.floor(Math.random() * (60 - 25) + 25);
                device.unit = '%'
            }

            this.devices.push(device);
        }

        this.storage.ready().then(() => {
            this.storage.get('access_token')
            .then(accessToken => this.accessToken = accessToken);
            this.storage.get('user_token')
            .then(userToken => this.userToken = userToken);
        });
    }

    showDevice(device) {
        this.modalCtrl.create(ShowDevicePage, {device: device}).present();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DevicesPage');
    }

}
