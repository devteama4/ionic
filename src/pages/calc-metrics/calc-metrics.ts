import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/operators/map';

import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the CalcMetricsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-calc-metrics',
    templateUrl: 'calc-metrics.html',
})
export class CalcMetricsPage {

    calcMetrics: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: Http,
        public toastCtrl: ToastController,
        public data: DataProvider) {

        this.data.getCalcMetrics().then(data => {
            this.calcMetrics = data;
        }).catch(error => {
            console.log(error);
        });
    }

}
