import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalcMetricsPage } from './calc-metrics';

@NgModule({
  declarations: [
    CalcMetricsPage,
  ],
  imports: [
    IonicPageModule.forChild(CalcMetricsPage),
  ],
})
export class CalcMetricsPageModule {}
