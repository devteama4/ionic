import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { LoginProvider } from '../../providers/login/login';
import * as decode from 'jwt-decode';

import { DevicesPage } from '../../pages/devices/devices';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {

    accessToken: any;
    refreshToken: any;
    decodedAccessToken: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public loginProvider: LoginProvider,
        public menuCtrl: MenuController,
        public loadingCtrl: LoadingController,
        public storage: Storage) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad HomePage');
    }

    ionViewDidEnter() {
        this.menuCtrl.swipeEnable(false);
    }

    login() {
        let loading = this.loadingCtrl.create({'enableBackdropDismiss': true, 'content': 'Please wait...'});
        
        this.loginProvider.login()
        .then(data => {

            loading.present();
            this.loginProvider.getToken(data)
            .then(token => {
                this.accessToken = JSON.parse(JSON.stringify(token)).access_token;
                this.refreshToken = JSON.parse(JSON.stringify(token)).refresh_token;
                this.decodedAccessToken = decode(this.accessToken);
            })
            .then(() => this.storage.set('access_token', this.accessToken))
            .then(() => this.storage.set('refresh_token', this.refreshToken)) 
            .then(() => this.storage.set('user_token', this.decodedAccessToken.sub))
            .then(() => {
                loading.dismiss();
                this.navCtrl.setRoot(DevicesPage, {}, {animate: true, direction: 'forward'});
            }).catch(error => {
                loading.dismiss();
                console.log('error when retrieving token ', error);
            });

        }).catch(error => {
            loading.dismiss();
            console.log('error when logging in ', error);
        });

        //this.navCtrl.setRoot(DevicesPage, {}, {animate: true, direction: 'forward'});

    }

    register() {
        this.loginProvider.register().then(data => {
            console.log(data); 
        })
    }

}
