import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowDevicePage } from './show-device';

@NgModule({
  declarations: [
    ShowDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(ShowDevicePage),
  ],
})
export class ShowDevicePageModule {}
