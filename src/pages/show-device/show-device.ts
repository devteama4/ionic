import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Toast } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the ShowDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-show-device',
    templateUrl: 'show-device.html',
})
export class ShowDevicePage {

    device: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private view: ViewController,
        public data: DataProvider,
        public toastCtrl: ToastController) {

        this.device = navParams.get('device');
    }

    turnOn() {
        this.data.device('h').then(data => {
            this.toastCtrl.create({
                message: 'Device LED → ON',
                position: 'bottom',
                duration: 2000,
                showCloseButton: true
            }).present();
        }, error => {
            this.toastCtrl.create({
                message: 'ERROR',
                position: 'bottom',
                duration: 2000,
                showCloseButton: true
            }).present();
        });
    }

    turnOff() {
        this.data.device('l').then(data => {
            this.toastCtrl.create({
                message: 'Device LED → OFF',
                position: 'bottom',
                duration: 2000,
                showCloseButton: true
            }).present();
        }, error => {
            this.toastCtrl.create({
                message: 'ERROR',
                position: 'bottom',
                duration: 2000,
                showCloseButton: true
            }).present();
        });
    }

    closeModal() {
        this.view.dismiss();
    }
}
