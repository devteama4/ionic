import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { DevicesPage } from '../pages/devices/devices'
import { CalcMetricsPage } from '../pages/calc-metrics/calc-metrics'; 

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) nav: Nav;
    rootPage:any = HomePage;

    pages: Array<{title: string, component: any, icon: string}>;

    constructor(
        platform: Platform,
        statusBar: StatusBar,
        splashScreen: SplashScreen,
        private storage: Storage) {

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            this.storage.clear();
        });

        this.pages = [
            { title: 'Devices', component: DevicesPage, icon: 'phone-portrait' },
            { title: 'Calc. Metrics', component: CalcMetricsPage, icon: 'stats' }
        ]
    }

    openPage(page) {
        this.nav.setRoot(page.component);
    }

    logout() {
        this.storage.clear()
        .then(() => this.nav.setRoot(HomePage));
    }
}

