import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicStorageModule } from '@ionic/storage';

import { LoginProvider } from '../providers/login/login';
import { DataProvider } from '../providers/data/data';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DevicesPage } from '../pages/devices/devices';
import { ShowDevicePage } from '../pages/show-device/show-device';
import { CalcMetricsPage } from '../pages/calc-metrics/calc-metrics';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DevicesPage,
    ShowDevicePage,
    CalcMetricsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DevicesPage,
    ShowDevicePage,
    CalcMetricsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginProvider,
    InAppBrowser,
    DataProvider
  ]
})
export class AppModule {}
